# from fpdf import FPDF

# TABLE_DATA = [
#     ("First name", "Last name", "Age", "City"),
#     ("Jules", "Smith", "34", "San Juan"),
#     ("Mary", "Ramos", "45", "Orlando"),
#     ("Carlson", "Banks", "19", "Los Angeles"),
#     ("Lucas", "Cimon", "31", "Saint-Mathurin-sur-Loire"),
# ]
# print(type(TABLE_DATA))
# pdf = FPDF()
# pdf.add_page()
# pdf.set_font("Times", size=16)
# pdf.image("https://bucket.dataiotapp.com/675a32a3-de3c-4b21-8577-2dbac0786756/BBO/2024-01-05/V/DJI_20231110112137_0009_V.JPG", x=20, y=60)
# with pdf.table() as table:
#     for data_row in TABLE_DATA:
#         row = table.row()
#         for datum in data_row:
#             row.cell(datum)
# pdf.output('table.pdf')
from PyPDF2 import PdfReader, PdfWriter
from io import BytesIO
from fpdf import FPDF
import time
from PIL import Image
import requests
def merge_pdfs(pdf1_bytesio, pdf2_bytesio):
    reader1 = PdfReader(pdf1_bytesio)
    reader2 = PdfReader(pdf2_bytesio)
    writer = PdfWriter()
    for page_num in range(len(reader1.pages)):
        writer.add_page(reader1.pages[page_num])
    for page_num in range(len(reader2.pages)):
        writer.add_page(reader2.pages[page_num])
    merged_pdf_bytesio = BytesIO()
    writer.write(merged_pdf_bytesio)
    merged_pdf_bytesio.seek(0) 
    return merged_pdf_bytesio

def pdf_1():
    pdf_buf1 = BytesIO()
    pdf1 = FPDF()
    pdf1.add_page()
    pdf1.set_font("helvetica", "B", 16)
    pdf1.cell(40, 10, "1231321231231231-----")
    pdf1.output(pdf_buf1)
    pdf_buf1.seek(0)
    print(type(pdf_buf1))
    return pdf_buf1

def pdf_2():
    pdf2 = FPDF()
    pdf_buf2 = BytesIO()
    pdf2.add_page()
    pdf2.set_font("helvetica", "B", 16)
    pdf2.cell(40, 10, "Another Pagssdfeefee!")
    pdf2.add_page()
    pdf2.set_font("helvetica", "B", 16)
    image_url = "https://bucket.dataiotapp.com/675a32a3-de3c-4b21-8577-2dbac0786756/BBO/2024-01-05/V/DJI_20231110112135_0007_V.JPG"
    response = requests.get(image_url)
    img1 = Image.open(BytesIO(response.content))
    img = img1.resize((300, 400), resample=Image.NEAREST)
    pdf2.image(img, x=80, y=100)
    pdf2.add_page()
    img = img1.resize((300, 400), resample=Image.NEAREST)
    pdf2.image(img, x=80, y=100)
    pdf2.cell(40, 10, "Another Pagssdsdfeeeeefeefee!")
    pdf2.output(pdf_buf2)
    pdf_buf2.seek(0)
    return pdf_buf2

print(type(pdf_2()))
merged_pdf_bytesio = BytesIO()
merged_pdf_bytesio = pdf_1()
for i in range(2):
    merged_pdf_bytesio = merge_pdfs(merged_pdf_bytesio, pdf_2())
    time.sleep(1)
    print(i)

with open("merged_output1.pdf", "wb") as f:
    f.write(merged_pdf_bytesio.getbuffer())