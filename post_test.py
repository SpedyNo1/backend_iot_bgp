import psycopg2

DB_HOST = "localhost"
DB_NAME = "mydatabase"
DB_USER = "myuser"
DB_PASSWORD = "mypassword"
DB_PORT = "5432"

def query_postgres(query, params=None):
    conn = psycopg2.connect(
        host=DB_HOST,
        database=DB_NAME,
        user=DB_USER,
        password=DB_PASSWORD,
        port=DB_PORT
    )
    cur = conn.cursor()
    try:
        cur.execute(query, params)
        rows = cur.fetchall()
        return rows
    except Exception as e:
        print(f"Error executing query: {e}")
    finally:
        cur.close()
        conn.close()

query = "SELECT id, name, price FROM public.products;"
a = query_postgres(query)
print(type(a))
print((a))
