from PyPDF2 import PdfReader, PdfWriter
from fpdf.fonts import FontFace
from io import BytesIO
from fpdf import FPDF
import redis
import requests
from io import BytesIO
from PIL import Image
import pandas as pd
# def merge_pdfs(pdf1_bytesio, pdf2_bytesio):
#     reader1 = PdfReader(pdf1_bytesio)
#     reader2 = PdfReader(pdf2_bytesio)
#     writer = PdfWriter()
#     for page_num in range(len(reader1.pages)):
#         writer.add_page(reader1.pages[page_num])
#     for page_num in range(len(reader2.pages)):
#         writer.add_page(reader2.pages[page_num])
#     merged_pdf_bytesio = BytesIO()
#     writer.write(merged_pdf_bytesio)
#     merged_pdf_bytesio.seek(0) 
#     return merged_pdf_bytesio

# # def pdf_gen():
# #     pdf_buf2 = BytesIO()
# #     pdf2 = FPDF()
# #     pdf2.add_page()
# #     pdf2.set_font("helvetica", "B", 16)
# #     pdf2.cell(40, 10, "Another Pagssdfeefee!")
# #     pdf2.add_page()
# #     pdf2.set_font("helvetica", "B", 16)
# #     pdf2.cell(40, 10, "Another Pagssdsdfeeeeefeefee!")
# #     pdf2.output(pdf_buf2)
# #     pdf_buf2.seek(0)
# #     return pdf_buf2
# blueby = (40, 66, 130)
# bluebu = (30, 36, 51)
# blue = (26, 50, 72)
# low_blue = (49, 64, 92)
# white = (255, 255, 255)
# red = (231, 76, 60)
# yellow = (212, 177, 61)
# orange = (230, 126, 34)
# black = (0,0,0)
# class PDF(FPDF):
#     def __init__(self,image_name, **kwargs):
#         super(PDF, self).__init__(**kwargs)
#         self.add_font('PatPong', '', 'domain/asset/fonts/DSN PATPONG.TTF', uni=True)
#         self.add_font('Boy', '', 'domain/asset/fonts/DSN PATPONG EXTEND.TTF', uni=True)
#         self.add_font('fuse', '', 'domain/asset/fonts/ARIAL.TTF', uni=True)
#         self.add_font('nice', '', 'domain/asset/fonts/ARIALBD.TTF', uni=True)
#         self.image_name = image_name
       
#     def header(self):
#         self.image(self.image_name, 10, 17, 50)
#         self.set_y(10)
#         self.set_font('nice', '', 15)
#         self.set_text_color(bluebu)
#         self.cell(0, 27, 'PV Inspection Report',align='R')
#         self.set_line_width(0.5)
#         self.set_draw_color(0, 0, 0)
#         self.line(10, 35, 200, 35)
#         self.ln(10)
     
        
#     def footer(self):
#         # Set position of the footer
#         self.set_y(-20)
#         # Line
#         vars# Draw a thick line at the top of the footer
#         self.set_line_width(0.5)  # Set the line width to 1.0
#         self.set_draw_color(0, 0, 0)  # Set the line color to black
#         self.line(10, self.get_y()-5, 200, self.get_y()-5)  # Draw the line
#         # Set font
#         self.set_font('nice', '', 8)
#         # Set font color
#         self.set_text_color(0, 0, 0)
#         # Page number
#         self.cell(0, 10, f'Page {self.page_no()}/{{nb}}', align='C')
        
#         # inspectors information
#         self.set_font('nice', '', 12)
#         self.set_text_color(blueby)
#         self.set_x(10)
#         self.cell(0, 5, 'Inspector', ln=1,align='L')
        
#         self.set_font('nice', '', 8)
#         self.set_text_color(0, 0, 0)
#         self.cell(0, 5, 'Boy Loveteen', ln=1,align='L') #name
#         self.cell(0, 5, 'Tel: 0623134646',align='L') #tel
        
#         # Drone AI address
#         self.set_font('nice', '', 12)
#         self.set_text_color(blueby)
#         self.set_y(-20)
#         self.cell(0, 5, 'Drone AI Company Limited', ln=1, align='R')
        
#         text = '972 Rimklongsamsane Rd(Rama9),\n Bangkapi, Huay Kwang, Bangkok, 10310, Thailand'
#         self.set_font('helvetica', 'B', 8)
#         self.set_text_color(0, 0, 0)
#         self.multi_cell(0, 5, text, ln=1,align='R')
        

#     def table_member(self, data):
#         self.add_table(data, H=47, clm=[50, 50, 35, 35, 20])
            
            
#     def table_gf(self, data):
#         a = 190/7
#         self.add_table(data, H=72, clm=[a,a,a,a,a,a,a])
            
            
#     def table_dji(self, data):
#         self.add_table(data, H=86, clm=[31.67, 63.33, 31.67, 31.67, 31.67])
        
    
#     def table_pvs(self, data):
#         self.add_table(data, H=110, clm=[95, 31.67, 31.67, 31.67])

    
#     def table_inr(self, data):
#         a = 190/6
#         self.add_table(data, H=215, clm=[a,a,a,a,a,a])           
            
            
#     def table_rec(self, data):
#         self.add_table(data, H=230, clm=[38, 76, 76])
            
            
#     def add_table(self, data, H, clm):
#         self.set_y(H)
#         table_width = 190  # Total table width
#         x_start = (self.w - table_width) / 2
        
#         self.set_x(x_start)
        
#         self.set_fill_color(blueby)
#         self.set_text_color(255, 255, 255)
#         self.set_font('nice', '', 8)
        
#         # Table Header
#         col_widths = clm  
#         for i, header in enumerate(data[0]):
#             self.cell(col_widths[i], 6, header, border=1, fill=True, align='C')
#         self.ln()
        
#         # Table Rows
#         self.set_fill_color(240, 240, 240)
#         self.set_text_color(0, 0, 0)
#         self.set_font('nice', '', 8)
        
#         for row in data[1:]:
#             self.set_x(x_start)
#             for i, datum in enumerate(row):
#                 if datum == '1':  # Check if the value is 1
#                     self.set_fill_color(255, 0, 0)  # Red color
                    
#                 elif datum == '2':  # Check if the value is 2
#                     self.set_fill_color(255, 165, 0)  # Orange color
                    
#                 elif datum == '3':  # Check if the value is 3
#                     self.set_fill_color(255, 255, 0)  # Yellow color
                    
#                 else:
#                     self.set_fill_color(240, 240, 240)  # Default color
                
#                 self.cell(col_widths[i], 6, datum, border=1, fill=True, align='C')
#             self.ln()          

# # --------------------------------------------------------------------------

# def pdf_testt(data:list,sumlist:list):
#     outputt = BytesIO()
#     sumlist.extend(data)
#     for feature in (data):
#         if isinstance(feature['properties'], dict) and isinstance(feature['geometry'], dict):
#             names = feature['properties']['tag']
#             photo = feature['properties']['var']
#             photy = "https://img2.pic.in.th/pic/DJI_20240510125544_0001_T.jpg"
#             pdf = PDF(orientation='P', unit='mm', format='A4', image_name='domain/asset/drone-AI-removebg.png')
#             pdf.set_auto_page_break(auto=True, margin=20)
#             pdf.add_page()
#             pdf.set_font('nice', '', 12)
#             pdf.set_text_color(bluebu)
#             pdf.set_y(54)
#             pdf.cell(0, 25, 'Parameters & Device Detail')
#             pdf.set_y(92)
#             pdf.cell(0, 25, 'PV Specification')
#             pdf.set_y(195)
#             pdf.cell(0, 25, 'Inspection Result')
#             pdf.set_y(250)
#             pdf.set_font('nice', '', 8)
#             pdf.cell(0, 25,'Recommend: After check this problem already, Please inspect this point with thermal camera again.')
#             h1 = [("IR Image", "Visible Image", "Latitude", "Longitude", "Altitude")]
#             h2 = [("Data", "Time", "Emissivity", "Humidity (%RH)", "Ambient-Temp(°C)", "Wind Speed (m/s)", "IRR (W/m²)")]
#             h3 = [("Device", "Devise S/N", "IFOV(mm)", "MFOV(mm)", "Accuracy(°C)")]
#             h4 = [("Component","Operating Max T (°C)","Operating Min T (°C)","PV Spot Size (mm)")]
#             h5 = [("Id","Type","Hot Cell Temp (°C)","Allowable Temp (°C)","(ΔT) Delta T (°C)","Priority")]
#             h6 = [("Priority","ΔT (Delta T) Compared with allowable temperature","Recommend Action")]
#             TABLE_DATA1 = [
#                 (feature['properties']['url'].split('/')[-1],
#                 feature['properties']['var'].split('/')[-1],
#                 str(feature['geometry']['coordinates'][1]),
#                 str(feature['geometry']['coordinates'][0]),
#                 "22.0")
#             ]

#             TABLE_G = [
#                 (feature['properties']['Time'],
#                 feature['properties']['Date'],
#                 "0.95",
#                 "55.0",
#                 "37.0",
#                 "1.0",
#                 "855")
#             ]

#             TABLE_T = [
#                 (feature['properties']['Device'],
#                 feature['properties']['Device_id'],
#                 feature['properties']['IFOV'],
#                 feature['properties']['MFOV'],
#                 feature['properties']['ACC']
#                 )
#             ]

#             TABLE_PVS = [
#                 (feature['properties']['pv_brand'],
#                 str(feature['properties']['normal_temp']),
#                 feature['properties']['pv_min'],
#                 feature['properties']['cell_size']
#                 )
#             ]
#             TABLE_REC = [
#                 ("1", "ΔT ≥ 15°C", "Major Discrepancy. Repair as possible"),
#                 ("2", "0°C < ΔT > 15°C", "Indicates probable deficiency. Repair as time permits"),
#                 ("3", "Lower than maximum temperature allowable", "The equipment is available for continued operate")
#             ]
            
            
#             pv_max = feature['properties']['normal_temp']
#             max_temp = feature['properties']['maxTemperature']
            
#             delta_t = round(float(pv_max)-float(max_temp),2)

#             if delta_t >= 0:
#                 delta_t = f"< {str(abs(delta_t))}"
#             else:
#                 delta_t = f"> {str(abs(delta_t))}"
            
#             TABLE_INR = [
#                 (feature['properties']['tag'],
#                 feature['properties']['hotspot_type'],
#                 str(max_temp),
#                 str(pv_max),
#                 str(delta_t),
#                 str(feature['properties']['priority'])
#                 )
#             ]
#             h1s = h1.copy()
#             h2s = h2.copy()
#             h3s = h3.copy()
#             h4s = h4.copy()
#             h5s = h5.copy()
#             h6s = h6.copy()

#             h1s.extend(TABLE_DATA1)
#             h2s.extend(TABLE_G)
#             h3s.extend(TABLE_T)
#             h4s.extend(TABLE_PVS)
#             h5s.extend(TABLE_INR)
#             h6s.extend(TABLE_REC)

#             pdf.table_member(h1s)
#             pdf.table_gf(h2s)
#             pdf.table_dji(h3s)
#             pdf.table_pvs(h4s)
#             pdf.table_inr(h5s)
#             pdf.table_rec(h6s)
#             pdf.set_y(29)
#             pdf.set_font('nice', '', 14)
#             pdf.set_text_color(bluebu)
#             pdf.cell(0, 25,"BBO - " + names)
#             pdf.set_y(90)
#             try:
#                 response = requests.get(photo)
#                 if response.status_code == 200:
#                     image_file = Image.open(BytesIO(response.content))
#                     image_file = image_file.crop((10, 10, 490, 490)).resize((800, 600))
#                     pdf.image(image_file, 10, 130, 90)
#                 else:
#                     print(f"Error fetching image from URL: {photo}")
#             except Exception as e:
#                 print(f"Error adding image for feature : {e}")
#             try:
#                 response = requests.get(photy)
#                 if response.status_code == 200:
#                     image_file = BytesIO(response.content)
#                     pdf.image(image_file, 110, 130, 90)
#                 else:
#                     print(f"Error fetching image from URL: {photy}")
#             except Exception as e:
#                 print(f"Error adding image for feature : {e}")
#         else:
#             print(f"Feature  has invalid properties or geometry structure")

#         pdf.output(outputt)
#         outputt.seek(0)
#     return outputt,sumlist
blueby = (40, 66, 130)
bluebu = (30, 36, 51)
blue = (26, 50, 72)
low_blue = (49, 64, 92)
white = (255, 255, 255)
red = (231, 76, 60)
yellow = (212, 177, 61)
orange = (230, 126, 34)
black = (0,0,0)
class PDF(FPDF):
    def __init__(self,image_name,page_number, **kwargs):
        super(PDF, self).__init__(**kwargs)
        self.add_font('PatPong', '', 'domain/asset/fonts/DSN PATPONG.TTF', uni=True)
        self.add_font('Boy', '', 'domain/asset/fonts/DSN PATPONG EXTEND.TTF', uni=True)
        self.add_font('fuse', '', 'domain/asset/fonts/ARIAL.TTF', uni=True)
        self.add_font('nice', '', 'domain/asset/fonts/ARIALBD.TTF', uni=True)
        self.add_font('Roboto', 'B', 'domain/asset/fonts/Roboto-Bold.ttf')
        self.set_font('Roboto', 'B', 8)
        self.image_name = image_name
        self.page_number = page_number

    def header(self):
        self.image(self.image_name, 10, 17, 50)
        self.set_y(10)
        self.set_font('nice', '', 15)
        self.set_text_color(bluebu)
        self.cell(0, 27, 'PV Inspection Report',align='R')
        self.set_line_width(0.5)
        self.set_draw_color(0, 0, 0)
        self.line(10, 35, 200, 35)
        self.ln(10)
          
    def footer(self):
        self.set_y(-20)
        vars
        self.set_line_width(0.5)  
        self.set_draw_color(0, 0, 0)  
        self.line(10, self.get_y()-5, 200, self.get_y()-5) 
        self.set_font('nice', '', 8)
        self.set_text_color(0, 0, 0)
        self.cell(0, 10, f'Page {self.page_number}', align='C')
        self.set_font('nice', '', 12)
        self.set_text_color(blueby)
        self.set_x(10)
        self.cell(0, 5, 'Inspector', ln=1,align='L')
        self.set_font('nice', '', 8)
        self.set_text_color(0, 0, 0)
        self.cell(0, 5, 'Boy Loveteen', ln=1,align='L') #name
        self.cell(0, 5, 'Tel: 0623134646',align='L') #tel
        self.set_font('nice', '', 12)
        self.set_text_color(blueby)
        self.set_y(-20)
        self.cell(0, 5, 'Drone AI Company Limited', ln=1, align='R')
        text = '972 Rimklongsamsane Rd(Rama9),\n Bangkapi, Huay Kwang, Bangkok, 10310, Thailand'
        self.set_font('helvetica', 'B', 8)
        self.set_text_color(0, 0, 0)
        self.multi_cell(0, 5, text, ln=1,align='R')
        

    def table_member(self, data):
        self.add_table(data, H=47, clm=[50, 50, 35, 35, 20])
            
            
    def table_gf(self, data):
        a = 190/7
        self.add_table(data, H=72, clm=[a,a,a,a,a,a,a])
               
    def table_dji(self, data):
        self.add_table(data, H=86, clm=[31.67, 63.33, 31.67, 31.67, 31.67])
        
    def table_pvs(self, data):
        self.add_table(data, H=110, clm=[95, 31.67, 31.67, 31.67])

    def table_inr(self, data):
        a = 190/6
        self.add_table(data, H=215, clm=[a,a,a,a,a,a])           
            
            
    def table_rec(self, data):
        self.add_table(data, H=230, clm=[38, 76, 76])
            
            
    def add_table(self, data, H, clm):
        self.set_y(H)
        table_width = 190  # Total table width
        x_start = (self.w - table_width) / 2
        
        self.set_x(x_start)
        
        self.set_fill_color(blueby)
        self.set_text_color(255, 255, 255)
        self.set_font('nice', '', 8)
        
        # Table Header
        col_widths = clm  
        for i, header in enumerate(data[0]):
            self.cell(col_widths[i], 6, header, border=1, fill=True, align='C')
        self.ln()
        
        # Table Rows
        self.set_fill_color(240, 240, 240)
        self.set_text_color(0, 0, 0)
        self.set_font('nice', '', 8)
        
        for row in data[1:]:
            self.set_x(x_start)
            for i, datum in enumerate(row):
                if datum == '1':  # Check if the value is 1
                    self.set_fill_color(255, 0, 0)  # Red color
                    
                elif datum == '2':  # Check if the value is 2
                    self.set_fill_color(255, 165, 0)  # Orange color
                    
                elif datum == '3':  # Check if the value is 3
                    self.set_fill_color(255, 255, 0)  # Yellow color
                    
                else:
                    self.set_fill_color(240, 240, 240)  # Default color
                
                self.cell(col_widths[i], 6, datum, border=1, fill=True, align='C')
            self.ln()          

# --------------------------------------------------------------------------
def pdf_mock():
    pdf = PDF(orientation='P', unit='mm', format='A4', image_name='domain/asset/drone-AI-removebg.png',page_number = 1)
    pdf.add_page()
    pdf.set_font('helvetica', size=12)
    pdf.cell(text="hello world")
    return pdf

def pdf_testt(data:list,page_number:int,pdf):
    outputt = BytesIO()
    for feature in (data):
        if isinstance(feature['properties'], dict) and isinstance(feature['geometry'], dict):
            names = feature['properties']['tag']
            photo = feature['properties']['var']
            photy = "https://img2.pic.in.th/pic/DJI_20240510125544_0001_T.jpg"
            pdf.add_page()
            pdf.page_number = page_number
            pdf.set_font('nice', '', 12)
            pdf.set_text_color(bluebu)
            pdf.set_y(54)
            pdf.cell(0, 25, 'Parameters & Device Detail')
            pdf.set_y(92)
            pdf.cell(0, 25, 'PV Specification')
            pdf.set_y(195)
            pdf.cell(0, 25, 'Inspection Result')
            pdf.set_y(250)
            pdf.set_font('nice', '', 8)
            pdf.cell(0, 25,'Recommend: After check this problem already, Please inspect this point with thermal camera again.')
            h1 = [("IR Image", "Visible Image", "Latitude", "Longitude", "Altitude")]
            h2 = [("Data", "Time", "Emissivity", "Humidity (%RH)", "Ambient-Temp(°C)", "Wind Speed (m/s)", "IRR (W/m²)")]
            h3 = [("Device", "Devise S/N", "IFOV(mm)", "MFOV(mm)", "Accuracy(°C)")]
            h4 = [("Component","Operating Max T (°C)","Operating Min T (°C)","PV Spot Size (mm)")]
            h5 = [("Id","Type","Hot Cell Temp (°C)","Allowable Temp (°C)","(ΔT) Delta T (°C)","Priority")]
            h6 = [("Priority","ΔT (Delta T) Compared with allowable temperature","Recommend Action")]
            TABLE_DATA1 = [
                (feature['properties']['url'].split('/')[-1],
                feature['properties']['var'].split('/')[-1],
                str(feature['geometry']['coordinates'][1]),
                str(feature['geometry']['coordinates'][0]),
                "22.0")
            ]
            TABLE_G = [
                (feature['properties']['Time'],
                feature['properties']['Date'],
                "0.95",
                "55.0",
                "37.0",
                "1.0",
                "855")
            ]
            TABLE_T = [
                (feature['properties']['Device'],
                feature['properties']['Device_id'],
                feature['properties']['IFOV'],
                feature['properties']['MFOV'],
                feature['properties']['ACC']
                )
            ]
            TABLE_PVS = [
                (feature['properties']['pv_brand'],
                str(feature['properties']['normal_temp']),
                feature['properties']['pv_min'],
                feature['properties']['cell_size']
                )
            ]
            TABLE_REC = [
                ("1", "ΔT ≥ 15°C", "Major Discrepancy. Repair as possible"),
                ("2", "0°C < ΔT > 15°C", "Indicates probable deficiency. Repair as time permits"),
                ("3", "Lower than maximum temperature allowable", "The equipment is available for continued operate")
            ]
            
            
            pv_max = feature['properties']['normal_temp']
            max_temp = feature['properties']['maxTemperature']
            
            delta_t = round(float(pv_max)-float(max_temp),2)

            if delta_t >= 0:
                delta_t = f"< {str(abs(delta_t))}"
            else:
                delta_t = f"> {str(abs(delta_t))}"
            
            TABLE_INR = [
                (feature['properties']['tag'],
                feature['properties']['hotspot_type'],
                str(max_temp),
                str(pv_max),
                str(delta_t),
                str(feature['properties']['priority'])
                )
            ]
            h1s = h1.copy()
            h2s = h2.copy()
            h3s = h3.copy()
            h4s = h4.copy()
            h5s = h5.copy()
            h6s = h6.copy()
            h1s.extend(TABLE_DATA1)
            h2s.extend(TABLE_G)
            h3s.extend(TABLE_T)
            h4s.extend(TABLE_PVS)
            h5s.extend(TABLE_INR)
            h6s.extend(TABLE_REC)
            pdf.table_member(h1s)
            pdf.table_gf(h2s)
            pdf.table_dji(h3s)
            pdf.table_pvs(h4s)
            pdf.table_inr(h5s)
            pdf.table_rec(h6s)
            pdf.set_y(29)
            pdf.set_font('nice', '', 14)
            pdf.set_text_color(bluebu)
            pdf.cell(0, 25,"BBO - " + names)
            pdf.set_y(90)
            # try:
            #     response = requests.get(photo)
            #     if response.status_code == 200:
            #         image_file = Image.open(BytesIO(response.content))
            #         image_file = image_file.crop((10, 10, 490, 490)).resize((800, 600))
            #         pdf.image(image_file, 10, 130, 90)
            #     else:
            #         print(f"Error fetching image from URL: {photo}")
            # except Exception as e:
            #     print(f"Error adding image for feature : {e}")
            # try:
            #     response = requests.get(photy)
            #     if response.status_code == 200:
            #         image_file = BytesIO(response.content)
            #         pdf.image(image_file, 110, 130, 90)
            #     else:
            #         print(f"Error fetching image from URL: {photy}")
            # except Exception as e:
            #     print(f"Error adding image for feature : {e}")
        else:
            print(f"Feature  has invalid properties or geometry structure")
    return pdf,pdf.page_number

def split_list_into_sublists(lst, sublist_length):
    return [lst[i:i+sublist_length] for i in range(0, len(lst), sublist_length)]

def create_summary_report(data,params):
    outputt = BytesIO()
    blue = (26, 50, 72)
    low_blue = (49, 64, 92)
    white = (255, 255, 255)
    red = (231, 76, 60)
    yellow = (241, 196, 15)
    orange = (230, 126, 34)
    black = (0,0,0)
    def create_table(table_data,col_width = None,fill_color = blue):
        headings_style = FontFace(emphasis="BOLD", color=white, fill_color=fill_color)
        with pdf.table(text_align="CENTER",headings_style=headings_style,col_widths=col_width) as table:
            for data_row in table_data:
                row = table.row()
                # link = pdf.add_link(page=1)
                # pdf.cell(text="Internal link to first page", border=1, link=link)
                for (index,datum) in enumerate(data_row):
                    if index == 7 or index == 0:
                        if datum == 3:
                            style = FontFace(color=black, fill_color=yellow)
                        elif datum == 2:
                            style = FontFace(color=white, fill_color=orange)
                        elif datum == 1:
                            style = FontFace(color=white, fill_color=red)
                        else:
                            style = None 
                        row.cell(str(datum),style=style)
                    else:
                        row.cell(str(datum))
                        #link = pdf.add_link(page=link_nb)

    def create_table_1(link_p,table_data,col_width = None,fill_color = blue):
        headings_style = FontFace(emphasis="BOLD", color=white, fill_color=fill_color)
        with pdf.table(text_align="CENTER",headings_style=headings_style,col_widths=col_width) as table:
            # link_p = 2+buffer1
            for k in range(len(table_data)):
                row = table.row()
                link = pdf.add_link(page=link_p)
                # pdf.cell(text="Internal link to first page", border=1, link=link)
                if(k==0):
                    for (index,datum) in enumerate(table_data[k]):
                        if index == 7 or index == 0:
                            if datum == 3:
                                style = FontFace(color=black, fill_color=yellow)
                            elif datum == 2:
                                style = FontFace(color=white, fill_color=orange)
                            elif datum == 1:
                                style = FontFace(color=white, fill_color=red)
                            else:
                                style = None 
                            row.cell(str(datum))
                        else:
                            row.cell(str(datum))
                else:
                    for (index,datum) in enumerate(table_data[k]):
                        if index == 7 or index == 0:
                            if datum == 3:
                                style = FontFace(color=black, fill_color=yellow)
                            elif datum == 2:
                                style = FontFace(color=white, fill_color=orange)
                            elif datum == 1:
                                style = FontFace(color=white, fill_color=red)
                            else:
                                style = None 
                            row.cell(str(datum),style=style,link=link)
                        else:
                            row.cell(str(datum),link=link)
                    link_p += 1
        return link_p

    df = pd.DataFrame(data, columns=["Id","Latitude", "Longitude","Type","Hot Cell Temp (°C)","Allowable Temp (°C)","(ΔT) Delta T (°C)","Priority"])
    priority_df = df['Priority'].value_counts()
    type_df = df.groupby('Type')['Type'].count()
    PRIOR_DATA = list(zip(priority_df.index,[str(item) for item in priority_df.values]))
    PRIOR_DATA.insert(0,("Priority","Panels"))
    TYPE_DATA = list(zip(type_df.index,[str(item) for item in type_df.values]))
    TYPE_DATA.insert(0,("Defect Type","Panels"))
    pages = split_list_into_sublists(data,35)
    pdf = PDF(orientation='P', unit='mm', format='A4', image_name='domain/asset/drone-AI-removebg.png',page_number = 1)
    pdf.set_auto_page_break(auto=True, margin=20)
    pdf.page_number = 1
    pdf.add_page()
    # ------ Statistics Table -------
    pdf.ln(25)
    pdf.set_text_color(26, 50, 72)
    pdf.set_font('Roboto', 'B', 12)
    pdf.cell(text=f"{params}")
    pdf.set_text_color(0, 0, 0)
    pdf.set_font('Roboto', 'B', 8)
    pdf.ln(10)
    pdf.cell(text="Priority Summary")
    pdf.ln(5)
    create_table(tuple(PRIOR_DATA))
    pdf.ln(5)
    pdf.cell(text="Defect Type Summary")
    pdf.ln(5)
    create_table(tuple(TYPE_DATA),(2,1))
    buffer1 = len(pages)+2
    for i in range(len(pages)):
        buffer = pages[i]
        buffer = [("Id","Latitude", "Longitude","Type","Hot Cell Temp (°C)","Allowable Temp (°C)","(ΔT) Delta T (°C)","Priority")]+buffer
        print(tuple(buffer))
        print("---------------------")
        pdf.add_page()
        pdf.page_number = pdf.page_number + 1
        pdf.ln(23)
        buffer1 = create_table_1(buffer1,tuple(buffer),(2,2,2,2,1,1,1,1))
        pdf.ln(10)
    return pdf,pdf.page_number


def data_for_sumlist(data):
    data_list = data['features']
    summary_list = []   
    for j in range(len(data_list)):
        latitude = data_list[j]['geometry']['coordinates'][0]
        longitude = data_list[j]['geometry']['coordinates'][1]
        defect_type = data_list[j]['properties']['hotspot_type']
        maxTemperature = data_list[j]['properties']['maxTemperature']
        pv_max = data_list[j]['properties']['normal_temp']
        delta_t = round(float(pv_max)-float(maxTemperature),2)
        if delta_t >= 0:
            delta_t = f"< {str(abs(delta_t))}"
        else:
            delta_t = f"> {str(abs(delta_t))}"
        delta_t = round(float(pv_max)-float(maxTemperature),2)
        priority = data_list[j]['properties']['priority']
        data_tuple = (data_list[j]['properties']['tag'],str(latitude),str(longitude),defect_type,maxTemperature,pv_max,str(delta_t),priority)
        summary_list.append(data_tuple)
    return summary_list


