import json
import os
class json_test:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def read_json_file(file_path):
        if not os.path.exists(file_path):
            raise FileNotFoundError(f"The file {file_path} does not exist.")
        with open(file_path, 'r') as file:
            return json.load(file)