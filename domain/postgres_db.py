import psycopg2
class postgresDataBase:
    def __init__(self,DB_HOST,DB_NAME,DB_USER,DB_PASSWORD,DB_PORT) -> None:
        self.DB_HOST=DB_HOST
        self.DB_NAME=DB_NAME
        self.DB_USER=DB_USER
        self.DB_PASSWORD=DB_PASSWORD
        self.DB_PORT=DB_PORT
        
    def query_postgres(self,query):
        conn = psycopg2.connect(
            host=self.DB_HOST,
            database=self.DB_NAME,
            user=self.DB_USER,
            password=self.DB_PASSWORD,
            port=self.DB_PORT
        )
        cur = conn.cursor()
        try:
            cur.execute(query,None)
            rows = cur.fetchall()     
            return rows
        except Exception as e:
            print(f"Error executing query: {e}")
        finally:
            cur.close()
            conn.close()

