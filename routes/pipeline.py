from fastapi import APIRouter,HTTPException,BackgroundTasks,Response
from fastapi.responses import StreamingResponse,FileResponse
from influxdb_client.client.write_api import SYNCHRONOUS
from domain import *
import time
import json
import os
import asyncio
import redis
import uuid
from PyPDF2 import PdfReader, PdfWriter
from io import BytesIO
from fpdf import FPDF
from config import (
    URL,
    TOKEN,
    ORG,
    BROKER,
    PORT,
    TOPIC,
    CLIENT_ID,
    MQTT_USERNAME,
    MQTT_PASSWORD,
    TELEGRAF,
    DB_HOST,
    DB_NAME,
    DB_USER,
    DB_PASSWORD,
    DB_PORT)

Influx = InfluxDataBase(URL,TOKEN,ORG)
postgres = postgresDataBase(DB_HOST,DB_NAME,DB_USER,DB_PASSWORD,DB_PORT)
r = redis.Redis(host="192.168.1.140", port=6379)
#http://192.168.1.140/
data_json = json_test(23,23)
with open('routes\data.json') as f:
    data = json.load(f)

router = APIRouter(      
    prefix="/api",
    responses={ 
        404 : {
            'message': 'Not Found'
        }
    }
)

@router.get("/read/latest")
async def test():
    try:
        return Influx.read_data_latest()
    except:
        raise HTTPException(status_code=404, detail="Something has problem")

@router.get("/read/test")
async def test():
    try:
        return Influx.read_test()
    except:
        raise HTTPException(status_code=404, detail="Something has problem")


@router.get("/read/latest/bts")
async def test():
    try:
        return Influx.read_data_latest_bts()
    except:
        raise HTTPException(status_code=404, detail="Something has problem")
    

@router.get("/postgres")
async def postgres_q():
    try:
        query = "SELECT id, name, price FROM public.products;"
        print(postgres.query_postgres(query))
        return postgres.query_postgres(query)
    except:
        raise HTTPException(status_code=404, detail="Something has problem")
    
@router.get("/")
async def test1():
    try:
        r.set(f"fghfgh", 156)
        return "complete system eiei"
    except:
        raise HTTPException(status_code=404, detail="Something has problem")

@router.get("/test")
async def test2():
    data_mock =  [data['features'][0]]
    data_list = data['features']
    summary_list = []   
    for j in range(len(data_list)):
        latitude = data_list[j]['geometry']['coordinates'][0]
        longitude = data_list[j]['geometry']['coordinates'][1]
        defect_type = data_list[j]['properties']['hotspot_type']
        maxTemperature = data_list[j]['properties']['maxTemperature']
        pv_max = data_list[j]['properties']['normal_temp']
        delta_t = round(float(pv_max)-float(maxTemperature),2)
        if delta_t >= 0:
            delta_t = f"< {str(abs(delta_t))}"
        else:
            delta_t = f"> {str(abs(delta_t))}"
        delta_t = round(float(pv_max)-float(maxTemperature),2)
        priority = data_list[j]['properties']['priority']
        data_tuple = (data_list[j]['properties']['tag'],str(latitude),str(longitude),defect_type,maxTemperature,pv_max,str(delta_t),priority)
        summary_list.append(data_tuple)
    pdf_data,nb = create_summary_report(summary_list,"sdfsefe")
    for i in range(0,len(data['features'])):
        pdf_data,nb =  pdf_testt([data['features'][i]],nb+1,pdf_data)

    filename = "sdfsefsdf.pdf"
    headers = {
        "Content-Disposition": f"attachment; filename={filename}"
    }
    return Response(content=bytes(pdf_data.output()), media_type="application/pdf", headers=headers)


    # pdf = pdf_mock()
    # a = BytesIO()
    # pdf.output(a)
    # a.seek(0)
    # print(a.getbuffer())
    # with open("routes/merged_output1.pdf", "wb") as f:
    #     f.write(a.getbuffer())
    # response = Response(content=a.getbuffer(), media_type="application/pdf")
    # response.headers["Content-Disposition"] = f"attachment; filename=dsfsefsef.pdf"
    # return response

@router.get("/json")
async def test5():
    f = open('routes\data.json',) 
    data = json.load(f) 
    f.close() 
    return data

def pdf_generator(task_id: str,data_in):
    try:
        pdf_data,nb = create_summary_report(data_in,"sdfsefe")
        for i in range(0,len(data['features'])):
            pdf_data,nb =  pdf_testt([data['features'][i]],nb+1,pdf_data)
            r.set(f"task:{task_id}:progress", (i))

        r.set(f"task:{task_id}:status", "completed")
        r.set(f"task:{task_id}:result", bytes(pdf_data.output()))
    except Exception as e:
        r.set(f"task:{task_id}:status", "failed")
        r.set(f"task:{task_id}:error", str(e))

@router.get('/report/')
def create_panel_report(background_tasks: BackgroundTasks):
    task_id = str(uuid.uuid4())
    data_mock =  [data['features'][0]]
    data_list = data['features']
    summary_list = []   
    for j in range(len(data_list)):
        latitude = data_list[j]['geometry']['coordinates'][0]
        longitude = data_list[j]['geometry']['coordinates'][1]
        defect_type = data_list[j]['properties']['hotspot_type']
        maxTemperature = data_list[j]['properties']['maxTemperature']
        pv_max = data_list[j]['properties']['normal_temp']
        delta_t = round(float(pv_max)-float(maxTemperature),2)
        if delta_t >= 0:
            delta_t = f"< {str(abs(delta_t))}"
        else:
            delta_t = f"> {str(abs(delta_t))}"
        delta_t = round(float(pv_max)-float(maxTemperature),2)
        priority = data_list[j]['properties']['priority']
        data_tuple = (data_list[j]['properties']['tag'],str(latitude),str(longitude),defect_type,maxTemperature,pv_max,str(delta_t),priority)
        summary_list.append(data_tuple)
        
    r.set(f"task:{task_id}:status", "in_progress")
    background_tasks.add_task(pdf_generator, task_id, summary_list)
    return {"task_id": task_id}


@router.get('/report/status/{task_id}')
def get_report_status(task_id: str):
    progress = r.get(f"task:{task_id}:progress")
    status = r.get(f"task:{task_id}:status")
    if not status:
        raise HTTPException(status_code=404, detail="Task not found")
    return {"task_id": task_id, "status": status.decode('utf-8'),"progress": progress.decode('utf-8')}

@router.get('/report/download/{task_id}')
def download_report(task_id: str):
    status = r.get(f"task:{task_id}:status")
    if not status:
        raise HTTPException(status_code=404, detail="Task not found")
    if status.decode('utf-8') != "completed":
        raise HTTPException(status_code=400, detail="Task not completed")
    pdf_data = r.get(f"task:{task_id}:result")
    if not pdf_data:
        raise HTTPException(status_code=404, detail="PDF result not found")
    # response = Response(content=pdf_data, media_type="application/pdf")
    # response.headers["Content-Disposition"] = f"attachment; filename={task_id}"
    # return response
    filename = "sdfsefsdf.pdf"
    headers = {
        "Content-Disposition": f"attachment; filename={filename}"
    }
    return Response(content=pdf_data, media_type="application/pdf", headers=headers)
    # headers = {'Content-Disposition': f'attachment; filename="{task_id}.pdf"'}
    # return Response(pdf_data, headers=headers, media_type="application/pdf")

@router.get('/report/delete/{task_id}')
def delete_report(task_id: str):
    cursor, keys = r.scan(0, match=f"task:{task_id}*")
    print(keys)
    if keys:
        r.delete(*keys)
        code = (f"Deleted {len(keys)} keys with prefix '{task_id}'")
    else:
        code = (f"No keys found with prefix '{task_id}'")
    return code

